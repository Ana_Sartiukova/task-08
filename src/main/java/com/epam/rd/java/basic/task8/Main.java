package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Movie;
import com.epam.rd.java.basic.task8.util.Sorter;

import java.util.ArrayList;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		ArrayList<Movie> movies = domController.parseDocument();

		// sort (case 1)
		Sorter.sortByTitle(movies);

		// save
		String outputXmlFile = "output.dom.xml";
		DOMController.saveToXML(movies, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		movies = saxController.parseDocument(true);
		
		// sort  (case 2)
		Sorter.sortByDirector(movies);
		
		// save
		outputXmlFile = "output.sax.xml";
		DOMController.saveToXML(movies, outputXmlFile);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		movies = staxController.parseDocument();
		
		// sort  (case 3)
		Sorter.sortByYear(movies);
		
		// save
		outputXmlFile = "output.stax.xml";
		DOMController.saveToXML(movies, outputXmlFile);
	}

}
