package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Movie;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private final String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public ArrayList<Movie> parseDocument(boolean validate) throws ParserConfigurationException, SAXException {

			// obtain sax parser factory
			SAXParserFactory factory = SAXParserFactory.newInstance();

			// XML document contains namespaces
			factory.setNamespaceAware(true);

			// set validation over xml and xsd
			if (validate) {
				factory.setFeature("http://xml.org/sax/features/validation", true);
				factory.setFeature("http://apache.org/xml/features/validation/schema", true);
			}

			SAXParser parser = factory.newSAXParser();
			SAXHandler handler = new SAXHandler();
		try {
			parser.parse(xmlFileName, handler);
			return handler.getMovies();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}