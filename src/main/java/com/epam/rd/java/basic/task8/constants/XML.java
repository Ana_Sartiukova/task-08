package com.epam.rd.java.basic.task8.constants;

public enum XML {

     MOVIES("entity:movies "),
     MOVIE("movie"),
     TITLE("title"),
     DIRECTOR("director"),
     YEAR("year"),
     CAST("cast"),
     ACTOR_NAME("actorName"),
     GENRES("genres"),
     GENRE_NAME("genreName");

     private final String value;
     XML(String value) {
         this.value = value;
     }

     public boolean equalsTo(String name) {
         return value.equals(name);
     }

     public String value() {
         return value;
     }
}

