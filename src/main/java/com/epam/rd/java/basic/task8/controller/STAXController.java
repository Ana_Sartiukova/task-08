package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Movie;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;
	private final ArrayList<Movie> movies = new ArrayList<>();
	private Movie movie = null;
	private final ArrayList<String> cast = null;


	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public ArrayList<Movie> parseDocument() {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
			while (reader.hasNext()) {
				XMLEvent nextEvent = reader.nextEvent();
				if(nextEvent.isStartElement()){
					StartElement startElement = nextEvent.asStartElement();
					switch (startElement.getName().getLocalPart()) {
						case "movie":
							movie = new Movie();
							break;
						case "title":
							nextEvent = reader.nextEvent();
							movie.setTitle(nextEvent.asCharacters().getData());
							break;
						case "director":
							nextEvent = reader.nextEvent();
							movie.setDirector(nextEvent.asCharacters().getData());
							break;
						case "year":
							nextEvent = reader.nextEvent();
							movie.setYear(Integer.parseInt(nextEvent.asCharacters().getData()));
							break;
						case "actorName":
							nextEvent = reader.nextEvent();
							movie.getCast().add(nextEvent.asCharacters().getData());
							break;
						case "genreName":
							nextEvent = reader.nextEvent();
							movie.getGenres().add(nextEvent.asCharacters().getData());
							break;
					}
				}
				if (nextEvent.isEndElement()) {
					EndElement endElement = nextEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("movie")) {
						movies.add(movie);
					}
				}
			}
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
		return movies;
	}


}