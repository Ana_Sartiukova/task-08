package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;

public class Movie {
    protected String title;
    protected String director;
    protected ArrayList<String> cast;
    protected int year;
    protected ArrayList<String> genres;

    public Movie(String title, String director, ArrayList<String> cast, int year, ArrayList<String> genres) {
        this.title = title;
        this.director = director;
        this.cast = cast;
        this.year = year;
        this.genres = genres;
    }

    public Movie() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public ArrayList<String> getCast() {
        if (cast == null) {
            cast = new ArrayList<>();
        }
        return this.cast;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public ArrayList<String> getGenres() {
        if (genres == null) {
            genres = new ArrayList<>();
        }
        return this.genres;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", director='" + director + '\'' +
                ", cast=" + cast +
                ", year=" + year +
                ", genres=" + genres +
                '}';
    }
}
