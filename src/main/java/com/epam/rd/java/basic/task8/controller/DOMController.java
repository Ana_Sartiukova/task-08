package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entity.Movie;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;
	private final ArrayList<Movie> movies = new ArrayList<>();

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;

	}

	public ArrayList<Movie> parseDocument() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(xmlFileName);
			NodeList movieList = doc.getElementsByTagName("movie");
			for (int i=0; i<movieList.getLength(); i++) {
				Node m = movieList.item(i);
				if(m.getNodeType()==Node.ELEMENT_NODE) {
					Element movie = (Element) m;
					String title = movie.getElementsByTagName("title").item(0).getTextContent();
					String director = movie.getElementsByTagName("director").item(0).getTextContent();
					int year = Integer.parseInt(movie.getElementsByTagName("year").item(0).getTextContent());

					ArrayList<String> actors = new ArrayList<>();
					ArrayList<String> genres = new ArrayList<>();

					Element a = (Element) movie.getElementsByTagName("cast").item(0);
					NodeList cast = a.getElementsByTagName("actorName");
					for (int j=0; j<cast.getLength(); j++) {
						actors.add(cast.item(j).getTextContent());
					}

					Element g = (Element) movie.getElementsByTagName("genres").item(0);
					NodeList genresList = g.getElementsByTagName("genreName");
					for (int k=0; k<genresList.getLength(); k++) {
						genres.add(genresList.item(k).getTextContent());
					}

					Movie newMovie = new Movie(title, director, actors, year, genres);
					movies.add(newMovie);
				}

			}
			return movies;
		} catch (ParserConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void saveToXML(ArrayList<Movie> movies, String xmlFileName) throws ParserConfigurationException, TransformerException {
		saveToXML(addMoviesToDom(movies), xmlFileName);
	}

	public static void saveToXML(Document document, String xmlFileName) throws TransformerException {

		StreamResult result = new StreamResult(new File(xmlFileName));

		// set up transformation
		TransformerFactory tf = TransformerFactory.newInstance();
		javax.xml.transform.Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");

		// run transformation
		t.transform(new DOMSource(document), result);
	}

	public static Document addMoviesToDom(ArrayList<Movie> movies) throws ParserConfigurationException {

		OutputMedicineDOMController out = new OutputMedicineDOMController();
		return out.addMoviesToDom(movies);
	}
	private static class OutputMedicineDOMController {

		private Document document;

		public Document addMoviesToDom(ArrayList<Movie> movies) throws ParserConfigurationException {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			document = db.newDocument();

			Element moviesDom = document.createElementNS("task08/entity", "entity:movies");
			addMovieToDom(movies, moviesDom);
			document.appendChild(moviesDom);
			return document;
		}

		public void addMovieToDom(ArrayList<Movie> movies, Element moviesDom) {
			for (Movie movie: movies) {
				Element movieDom = document.createElement(XML.MOVIE.value());

				addTextToDom(movieDom, XML.TITLE.value(), movie.getTitle());
				addTextToDom(movieDom, XML.DIRECTOR.value(), movie.getDirector());


				Element castDom = document.createElement(XML.CAST.value());
				for (String actor : movie.getCast()) {
					addTextToDom(castDom, XML.ACTOR_NAME.value(), actor);
				}
				movieDom.appendChild(castDom);

				addTextToDom(movieDom, XML.YEAR.value(), String.valueOf(movie.getYear()));

				Element genresDom = document.createElement(XML.GENRES.value());
				for (String genreName : movie.getGenres()) {
					addTextToDom(genresDom, XML.GENRE_NAME.value(), genreName);
				}
				movieDom.appendChild(genresDom);

				moviesDom.appendChild(movieDom);
			}
		}

		private void addTextToDom(Element parentDom, String elementName, String data) {
			Element elementDom = document.createElement(elementName);
			elementDom.setTextContent(data);
			parentDom.appendChild(elementDom);
		}
	}



}
