package com.epam.rd.java.basic.task8.util;

import com.epam.rd.java.basic.task8.entity.Movie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public final class Sorter {

    public static final Comparator<Movie> COMPARE_BY_TITLE = new Comparator<>() {
        @Override
        public int compare(Movie o1, Movie o2) {
            int i = o1.getTitle().compareTo(o2.getTitle());
            if (i == 0) {
                return COMPARE_BY_YEAR.compare(o1, o2);
            }
            return i;
        }
    };

    public static final Comparator<Movie> COMPARE_BY_DIRECTOR = new Comparator<>() {
        @Override
        public int compare(Movie o1, Movie o2) {
            int i = o1.getDirector().compareTo(o2.getDirector());
            if (i == 0) {
                return COMPARE_BY_YEAR.compare(o1, o2);
            }
            return i;
        }
    };

    public static final Comparator<Movie> COMPARE_BY_YEAR = Comparator.comparingInt(Movie::getYear);


    private Sorter() {
    }

    public static void sortByTitle(ArrayList<Movie> movies) {
        movies.sort(COMPARE_BY_TITLE);
    }

    public static void sortByDirector(ArrayList<Movie> movies) {
        movies.sort(COMPARE_BY_DIRECTOR);
    }

    public static void sortByYear(ArrayList<Movie> movies) {
        movies.sort(COMPARE_BY_YEAR);
    }
}
