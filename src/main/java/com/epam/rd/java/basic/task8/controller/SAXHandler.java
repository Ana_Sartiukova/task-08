package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Movie;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;


import java.util.ArrayList;


public class SAXHandler extends DefaultHandler {
    ArrayList<Movie> movies;
    ArrayList<String> cast;
    ArrayList<String> genres;
    StringBuilder sb = new StringBuilder();

   @Override
    public void characters(char[] ch, int start, int length) {
       if (sb == null) {
           sb = new StringBuilder();
       } else {
           sb.append(ch, start, length);
       }
   }

   @Override
    public void startDocument(){
       movies = new ArrayList<>();
   }

   @Override
    public void startElement(String uri, String lName, String qName, Attributes attr) {
       switch (qName) {
           case "movie":
               movies.add(new Movie());
               break;
           case "title" :
           case "director":
           case "year":
           case "actorName":
           case "genreName":
               sb = new StringBuilder();
               break;
           case "cast":
               cast = new ArrayList<>();
               break;
           case "genres":
               genres = new ArrayList<>();
               break;
       }
    }

    @Override
    public void endElement(String uri, String lName, String qName) {
        switch (qName) {
            case "title":
                latestMovie().setTitle(sb.toString());
                break;
            case "director":
                latestMovie().setDirector(sb.toString());
                break;
            case "year":
                latestMovie().setYear(Integer.parseInt(sb.toString()));
                break;
            case "actorName":
                latestMovie().getCast().add(sb.toString());
                break;
            case "genreName":
                latestMovie().getGenres().add(sb.toString());
                break;
        }
    }

    private Movie latestMovie() {
       ArrayList<Movie> movieList = movies;
       int latestMovieIndex = movieList.size()-1;
       return movieList.get(latestMovieIndex);
    }

    public ArrayList<Movie> getMovies() {
       return movies;
    }
}
